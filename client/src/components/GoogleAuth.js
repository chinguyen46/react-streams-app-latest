import React from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from '../actions/index.js';

class GoogleAuth extends React.Component{

    componentDidMount(){
        window.gapi.load('client:auth2', () =>{ //available in window scope
            window.gapi.client.init({ //returns a promise
                clientId: '983715619364-5umb6ami1si9o922kgsgt6ordtj53fq2.apps.googleusercontent.com',
                scope: 'email'
            }).then(() =>{
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange); //invoke callback everytime user auth status is changed
            });
        }); 
    };

    onAuthChange = (isSignedIn) =>{ //arrow function so context is bound to component 
        isSignedIn ? this.props.signIn(this.auth.currentUser.get().getId()) : this.props.signOut();
    };

    onSignOutClick = () =>{
        this.auth.signOut();
    };

    onSignInClick = () =>{
        this.auth.signIn();
    };

    renderAuthButton(){
        if(this.props.isSignedIn === null){
            return null;
        }else if(this.props.isSignedIn){
            return (
                <button className="btn btn-danger" onClick={this.onSignOutClick}>  
                <i className="fab fa-google"></i>
                    Sign Out
                </button>
            )
        }else{
            return (
                <button className="btn btn-success" onClick={this.onSignInClick}>
                <i className="fab fa-google"></i>
                    Sign In with Google
                </button>
            )
        }
    };

    render(){
        return(
            <div>
                {this.renderAuthButton()}
                <h2>{this.props.userId}</h2>
            </div>
        )
    };
}; 

const mapStateToProps = (state) =>{
    return {
        isSignedIn: state.auth.isSignedIn,
        userId: state.auth.userId
    }
};

export default connect(mapStateToProps, {
    signIn: signIn,
    signOut: signOut
})(GoogleAuth);