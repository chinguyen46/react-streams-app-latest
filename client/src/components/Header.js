import React from 'react';
import { Link } from 'react-router-dom';
import GoogleAuth from './GoogleAuth.js';

const Header = () =>{
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/">Streamy</Link>
                <ul className="navbar-nav my-2 my-lg-0 ml-auto">
                    <li className="nav-item active">
                        <Link className="nav-link" to="/">All Streams</Link>
                    </li>
                    <li className="nav-item active">
                        <GoogleAuth />
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default Header;


