import React from 'react';
import { Field, reduxForm } from 'redux-form';

class StreamCreate extends React.Component{

    renderInput = (formProps) =>{ //formProps provided automatically by Redux Form since we did component={this.renderInput}
        return (
            <div className="form-group">
            <label>{formProps.label}</label>
            <input 
                autoComplete="off"
                className="form-control"
                onChange={formProps.input.onChange}
                value={formProps.input.value}
            />
            <div>{this.renderError(formProps.meta)}</div>
            </div>
        )
    };

    renderError(meta){
        if(meta.touched && meta.error){
            return (
            <div className="alert alert-warning mt-1" role="alert">{meta.error}</div>
            )
        }
    };

    onSubmit(formValues){
        console.log(formValues);
    };

    render(){
        return (
            <form className="mt-2" onSubmit={this.props.handleSubmit(this.onSubmit)}>
                <Field name="title" component={this.renderInput} label="Enter title"/> 
                <Field name="description" component={this.renderInput} label="Enter description"/> 
                <button className="btn btn-primary">Submit</button>
            </form>
        )
    };

};

const validate = (formValues) =>{
    const errors = {};

    if(!formValues.title){
        errors.title = 'You must enter a title';
    }
    if(!formValues.description){
        errors.description = 'You must enter a description';
    }

    return errors;
};

export default reduxForm({
    form: 'streamCreate',
    validate: validate
})(StreamCreate);