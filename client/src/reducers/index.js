import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'; //changing name of reducer to make it more explicit
import authReducer from './authReducer.js';

export default combineReducers({
    auth: authReducer,
    form: formReducer //must be assigned to form key
});